package com.w2a.API_Batch3.pojo;

public class TokenCreation {
	
	String username;
	String password;
	
	public TokenCreation(String username, String password) {
		this.username = username;
		this.password = password;		
	}
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}	
}
