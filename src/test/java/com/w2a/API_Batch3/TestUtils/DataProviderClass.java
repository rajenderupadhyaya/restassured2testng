package com.w2a.API_Batch3.TestUtils;

import java.lang.reflect.Method;
import java.util.Hashtable;

import org.testng.annotations.DataProvider;

import com.w2a.API_Batch3.setUp.APISetUp;

public class DataProviderClass extends APISetUp{
	
	@DataProvider(name = "dp")
	public Object[][] getData(Method m) {
		String sheetName = m.getName();
		int rows = excel1.getRowCount(sheetName);// 3
		int cols = excel1.getColumnCount(sheetName);// 2
		
	//	System.out.println("DataProviderClass  :  " + sheetName + " ====  "  + rows + " ::: " + cols); 
		 
		// int[][] array= new int[5][4];
		Object[][] data = new Object[rows - 1][1];// [2][1]
		// int[] array= new int[1][1];
		// array[0][0]=10
		Hashtable<String, String> table = null;
		for (int rowNum = 2; rowNum <= rows; rowNum++) {// rows=3
	
				table = new Hashtable<String, String>();
			for (int colNum = 0; colNum < cols; colNum++) {// cols=2
				table.put(excel1.getCellData(sheetName, colNum, 1), 
						excel1.getCellData(sheetName, colNum, rowNum));
				
			/*	System.out.println(rowNum + " :::::::  " + excel1.getCellData(sheetName, colNum, 1) + " :::::::  " 
						+ excel1.getCellData(sheetName, colNum, rowNum)); 
			*/
				
				// Commented by me as same array is overridden by HashTable twice. Its better to take 
				// data array outside. So that we can append HashTable with two entries in a single step.
				// data[rowNum - 2][0] = table;// {endPoint=/customer,expectedCode=200} 
				 
			}
			 // I have taken this below line out of above loop
			 data[rowNum - 2][0] = table;// {endPoint=/customer,expectedCode=200} 
			// System.out.println("DataProviderClass  :  " + data.length); 
		//	 System.out.println("DataProviderClass  :  " + table.entrySet()); 
			 table=null;
			// System.out.println("*******************DataProviderClass*******************************"); 
		}
		return data;
	}

}
