package com.w2a.API_Batch3.testCases;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Hashtable;

import org.aeonbits.owner.ConfigFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.w2a.API_Batch3.API.CustomerAPI;
import com.w2a.API_Batch3.TestUtils.ConfigProperty;
import com.w2a.API_Batch3.TestUtils.DataProviderClass;
import com.w2a.API_Batch3.TestUtils.DataProviderClass2;
import com.w2a.API_Batch3.setUp.APISetUp;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TestCreateCustomerAPI2 extends APISetUp {	
	
	@Test(dataProviderClass=DataProviderClass2.class,dataProvider="dp2",enabled=true)
	public void validateCreateCustomerAPIWithValidData(Hashtable<String, String> data)
	{
		testLevelLog.get().assignAuthor("Rahul");
		testLevelLog.get().assignCategory("Sanity");
		
		Response response=CustomerAPI.sendPostRequestWithValidDataWithArguments(data);
		response.prettyPrint();
		Assert.assertEquals(response.statusCode(),Integer.parseInt(data.get("expectedStatusCode")));
		Assert.assertEquals(response.jsonPath().get("email"), data.get("expectedEmail"));
		//Assert.assertEquals(response.statusLine(), "Created");
		// Try to fail assertion also by uncommenting below line to check how reports looks like
	    //Assert.fail();
		
	}
	
	@Test(enabled=false)
	public void m1() {
		
		Assert.fail();
	}
}
