package com.w2a.API_Batch3.TestUtils;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.w2a.API_Batch3.setUp.APISetUp;

import io.restassured.specification.RequestSpecification;

public class TestUtil extends APISetUp {

	public static RequestSpecification setFormParam(String arguments, RequestSpecification reqSpecs) {
		String[] listOfAruments = arguments.split(",");
		for (String singleArgumen : listOfAruments) {
			String[] keyValue = singleArgumen.split(":");
			{ // unneccessarily these curly braces used
				String key = keyValue[0];
				String value = keyValue[1];
				// reqSpecs.body(arg0)  // In case JSON payload goes in body 
				reqSpecs.formParam(key, value); // In this case JSON payload goes as Query parameter 
			} // unneccessarily these curly braces used

		}
		return reqSpecs;

	}
    
	// Archive Report1
	public static void archiveTestReport() {
		    System.out.println("Conflict in this line ==>> Accept Repository Change and Reject Local Change for this line only. Local changes at other place accept. Test Synchronize");
			String reportName = configProperty.getTestReportName();
			System.out.println("This new line will be added in Repo code");
			String lastTestReportFilePath = System.getProperty("user.dir")+"/src/test/resources/testReports/";
			String archiveReportPath = System.getProperty("user.dir")+"/src/test/resources/archivedTestReport/";

			Date date = new Date();
			SimpleDateFormat dateFormate = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
			String formatedDate = dateFormate.format(date);
			String archiveTestReportName = formatedDate + "_" + reportName;

			File oldReport = new File(lastTestReportFilePath + reportName);

			File newFile = new File(archiveReportPath + archiveTestReportName);
			
			System.out.println(oldReport.exists());
			
			if (oldReport.exists()) {
				System.out.println("inside if");
				oldReport.renameTo(newFile);
				oldReport.delete();
			}	
	}	
	
 }
