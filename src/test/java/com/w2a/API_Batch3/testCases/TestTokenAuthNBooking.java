package com.w2a.API_Batch3.testCases;

import static io.restassured.RestAssured.given;

import java.text.ParseException;
import java.util.Date;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.w2a.API_Batch3.TestUtils.TestUtil;
import com.w2a.API_Batch3.pojo.BookingDates;
import com.w2a.API_Batch3.pojo.PersonDetail;
import com.w2a.API_Batch3.pojo.PersonDetailAllBooking;
import com.w2a.API_Batch3.pojo.PersonDetailPartial;
import com.w2a.API_Batch3.pojo.TokenCreation;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TestTokenAuthNBooking {

	// I am running this as individual testng test and not through testng xml or
	// pom.xml

	// API used : https://restful-booker.herokuapp.com/apidoc/index.html
	
	@Test(priority = 0, enabled = true)
	public void getAllBooking(ITestContext iTestContext) {
		/*
		 * URI - https://restful-booker.herokuapp.com/booking/ 
		 * MethodType-GET 
		 * argument- firstname, lastname, checkin, checkout
		 * path parameter or URL parameter - id
		 * Header: Cookie - token
		 */
		PersonDetailAllBooking personDetailAllBooking = new PersonDetailAllBooking("URaj", "Upd", "2020-12-22", "2020-12-25");
	
		System.out.println("*************** Print Request *******0***********");
		RequestSpecification spec = given().contentType(ContentType.JSON).log().all().body(personDetailAllBooking);
		Response res = spec.get("https://restful-booker.herokuapp.com/booking/");
		System.out.println("*************** Print Response *******0***********");
		res.prettyPrint();
		// restful-booker.herokuapp API sending 200 while creating token.
		Assert.assertEquals(200,res.getStatusCode());		
	}	

	@Test(priority = 1, enabled = true)
	public void validateTokenCreation(ITestContext iTestContext) {
		/*
		 * URI - https://restful-booker.herokuapp.com/auth MethodType-POST argument-
		 * username, password
		 */
		TokenCreation tokenCreation = new TokenCreation("admin", "password123");
		System.out.println("*************** Print Request ********1**********");
		RequestSpecification spec = given().contentType(ContentType.JSON).log().all().body(tokenCreation);
		Response res = spec.post("https://restful-booker.herokuapp.com/auth");
		System.out.println("*************** Print Response ********1**********");
		res.prettyPrint();
		iTestContext.setAttribute("token", res.jsonPath().get("token"));
		System.out.println("*************** Token Start *******************");
		System.out.println(iTestContext.getAttribute("token"));
		// restful-booker.herokuapp API sending 200 while creating token.
		Assert.assertEquals(200,res.getStatusCode());
		System.out.println("*************** Token End *******************");
	}

	@Test(priority = 2, enabled = true)
	public void createBooking(ITestContext iTestContext) {
		/*
		 * URI - https://restful-booker.herokuapp.com/booking 
		 * MethodType-POST 
		 * argument- firstname, lastname, totalprice, depositpaid, bookingdates, additionalneeds
		 */
		PersonDetail personDetail = new PersonDetail("URaj", "Upd", 5678, true, "Breakfast");
		BookingDates bookingDates = new BookingDates("2020-12-21", "2020-12-24");
		personDetail.setBookingdates(bookingDates);
		System.out.println("*************** Print Request *******2***********");
		RequestSpecification spec = given().contentType(ContentType.JSON).log().all().body(personDetail);
		Response res = spec.post("https://restful-booker.herokuapp.com/booking");
		System.out.println("*************** Print Response *******2***********");
		res.prettyPrint();
		iTestContext.setAttribute("bookingid", res.jsonPath().get("bookingid"));
		// restful-booker.herokuapp API sending 200 while creating token.
		Assert.assertEquals(200,res.getStatusCode());
		Assert.assertEquals("Breakfast",res.jsonPath().get("booking.additionalneeds"));
		Assert.assertEquals(true,res.jsonPath().get("booking.depositpaid"));
	}
	
	@Test(priority=3,enabled=true) 
	public void getBooking(ITestContext iTestContext) { 
		/*
		 * URI - https://restful-booker.herokuapp.com/booking/:id 
		 * MethodType-GET 
		 * path parameter or URL parameter - id
		 */	   
		  System.out.println("*************** Print Request *********3*********");
		  RequestSpecification spec = given().contentType(ContentType.JSON).pathParam("id",iTestContext.getAttribute("bookingid")).log().all(); 
		  Response res = spec.get("https://restful-booker.herokuapp.com/booking/{id}");
		  System.out.println("*************** Print Response *********3*********");
		  res.prettyPrint();
		  Assert.assertEquals("Breakfast",res.jsonPath().get("additionalneeds"));
	}
	
	@Test(priority = 4,enabled = true)
	public void updateBooking(ITestContext iTestContext) {
		/*
		 * URI - https://restful-booker.herokuapp.com/booking/:id 
		 * MethodType-PUT 
		 * argument- firstname, lastname, totalprice, depositpaid, bookingdates, additionalneeds
		 * path parameter or URL parameter - id
		 * Header: Cookie - token
		 */
		PersonDetail personDetail = new PersonDetail("URaj", "Upd", 5678, true, "Breakfast123");
		BookingDates bookingDates = new BookingDates("2020-12-22", "2020-12-25");
		personDetail.setBookingdates(bookingDates);
		System.out.println("*************** Print Request *******4***********");
		RequestSpecification spec = given().contentType(ContentType.JSON).pathParam("id",iTestContext.getAttribute("bookingid")).cookie("token",iTestContext.getAttribute("token")).log().all().body(personDetail);
		Response res = spec.put("https://restful-booker.herokuapp.com/booking/{id}");
		System.out.println("*************** Print Response *******4***********");
		res.prettyPrint();
		// restful-booker.herokuapp API sending 200 while creating token.
		Assert.assertEquals(200,res.getStatusCode());
		Assert.assertEquals("Breakfast123",res.jsonPath().get("additionalneeds"));
		Assert.assertEquals(true,res.jsonPath().get("depositpaid"));
	}
	
	@Test(priority = 5, enabled = true)
	public void patchBooking(ITestContext iTestContext) {
		/*
		 * URI - https://restful-booker.herokuapp.com/booking/:id 
		 * MethodType-PATCH 
		 * argument- firstname, lastname
		 * path parameter or URL parameter - id
		 * Header: Cookie - token
		 */
		PersonDetailPartial personDetailPartial = new PersonDetailPartial("PopURaj", "PopUpd");
	
		System.out.println("*************** Print Request *******5***********");
		RequestSpecification spec = given().contentType(ContentType.JSON).pathParam("id",iTestContext.getAttribute("bookingid")).cookie("token",iTestContext.getAttribute("token")).log().all().body(personDetailPartial);
		Response res = spec.patch("https://restful-booker.herokuapp.com/booking/{id}");
		System.out.println("*************** Print Response *******5***********");
		res.prettyPrint();
		// restful-booker.herokuapp API sending 200 while creating token.
		Assert.assertEquals(200,res.getStatusCode());
		Assert.assertEquals("PopURaj",res.jsonPath().get("firstname"));
		Assert.assertEquals("PopUpd",res.jsonPath().get("lastname"));
	}	
	
	@Test(priority=6,enabled=true) 
	public void deleteBooking(ITestContext iTestContext) { 
		/*
		 * URI - https://restful-booker.herokuapp.com/booking/:id 
		 * MethodType-DELETE 
		 * path parameter or URL parameter - id
		 */	   
		  System.out.println("*************** Print Request *********6*********");
		  RequestSpecification spec = given().contentType(ContentType.JSON).pathParam("id",iTestContext.getAttribute("bookingid")).cookie("token",iTestContext.getAttribute("token")).log().all(); 
		  Response res = spec.delete("https://restful-booker.herokuapp.com/booking/{id}");
		  System.out.println("*************** Print Response *********6*********");
		  res.prettyPrint();
		  Assert.assertEquals(201,res.getStatusCode());		 
	}	
}
