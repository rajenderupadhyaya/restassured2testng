package com.w2a.API_Batch3.testCases;

import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Hashtable;

import org.aeonbits.owner.ConfigFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.w2a.API_Batch3.TestUtils.ConfigProperty;
import com.w2a.API_Batch3.TestUtils.DataProviderClass;
import com.w2a.API_Batch3.setUp.APISetUp;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TestCreateCustomerAPI extends APISetUp {	

	
	@Test(enabled=false)
	public void validateCreateCustomerAPIWithValidData()
	{
		// Before using owner API. See Next method for owner API.
		/*
		 * URI - https://api.stripe.com/v1/customers
		 * MethodType-POST
		 * argument- email, description, balance
		 * auth- Basic auth- sk_test_51Hnlk0Fa0vovYb1rLhaS1rr5Ny31ghapqVq4b3PirkbiVNhs1xxI2CF4MZotuQGCRpsm9x1hu3GHwqycxsOb3wnh003oL62vGb
	     */
		RequestSpecification spec = given().auth().basic("sk_test_51Hnlk0Fa0vovYb1rLhaS1rr5Ny31ghapqVq4b3PirkbiVNhs1xxI2CF4MZotuQGCRpsm9x1hu3GHwqycxsOb3wnh003oL62vGb", "").
				formParam("email","rajTest1@yahoo.com").formParam("description","Just Testing").formParam("balance",10000).log().all();			
        System.out.println("=================Call to API 99============================");
        System.out.println("=================Call to API 88============================");
		Response res = spec.post("https://api.stripe.com/v1/customers");
		res.prettyPrint();
		// Stripe API sending 200 while creating new customer instead of standard 201.
		Assert.assertEquals(200,res.getStatusCode());
	}
	
	// Generating Extent Report
	
	@Test(enabled=false)
	public void validateCreateCustomerAPIWithValidData1()
	{
		/*
		 * URI - https://api.stripe.com/v1/customers
		 * MethodType-POST
		 * argument- email, description, balance
		 * auth- Basic auth- configProperty.getSecretKey()
		 */
		
		testLevelLog.get().assignAuthor("Rajender");
		testLevelLog.get().assignCategory("Regression");
				
		RequestSpecification spec =  getRequestSpecification().formParam("email","rajTest2@yahoo.com").
				                     formParam("description","Just Testing").formParam("balance",10000).log().all();			
        System.out.println("=============Call to API 22 ================================");
		Response res = spec.post("/customers");
		
		testLevelLog.get().info(res.asString());
		
		res.prettyPrint();
		// Stripe API sending 200 while creating new customer instead of standard 201.
		Assert.assertEquals(200,res.getStatusCode());
	}
	
	// Using excel (xls) file and Generating Extent Report 
	// Warning : Method name and XLS sheet name should be same
	
	
	@Test(dataProviderClass=DataProviderClass.class,dataProvider="dp",enabled=false)
	public void validateCreateCustomerAPIXLS(Hashtable<String, String> data)
	{
		/*
		 * URI - https://api.stripe.com/v1/customers
		 * MethodType-POST
		 * argument- email, description, balance
		 * auth- Basic auth- configProperty.getSecretKey()
		 */
		
		// Warning : Method name and XLS sheet name should be same
		
		testLevelLog.get().assignAuthor("Rajender");
		testLevelLog.get().assignCategory("Regression");
				
		RequestSpecification spec =  getRequestSpecification().formParam("email",data.get("email")).
				                     formParam("description",data.get("description")).formParam("balance",10000).log().all();			
        System.out.println("=============Call to API 2 ================================");
		Response res = spec.post("/customers");
		
		testLevelLog.get().info(res.asString());
		
		res.prettyPrint();
		// Stripe API sending 200 while creating new customer instead of standard 201.
		Assert.assertEquals(200,res.getStatusCode());
	}
	
	//  Parsing JSON Response and Using excel (xls) file and Generating Extent Report 
	// Warning : Method name and XLS sheet name should be same
	     

		@Test(dataProviderClass=DataProviderClass.class,dataProvider="dp",enabled=false)
		public void parseJsonResponse(Hashtable<String, String> data)
		{
			/*
			 * URI - https://api.stripe.com/v1/customers
			 * MethodType-POST
			 * argument- email, description, balance
			 * auth- Basic auth- configProperty.getSecretKey()
			 */
			
			// Warning : Method name and XLS sheet name should be same
			
			testLevelLog.get().assignAuthor("Rajender");
			testLevelLog.get().assignCategory("Regression");
					
			RequestSpecification spec =  getRequestSpecification().formParam("email",data.get("email")).
					                     formParam("description",data.get("description")).formParam("balance",10000).log().all();			
	        System.out.println("=============Call to API 2 ================================");
			Response response = spec.post("/customers");
			
			// Fetch Response using "path"
			
			String email = response.path("email");
			System.out.println(email);
			
			String description = response.path("description");
			System.out.println(description);
			
			String footer = response.path("invoice_settings.footer");
			System.out.println(footer);
			
			// Fetch Response using "JsonPath"
			
			JsonPath jsonPath = new JsonPath(response.asString()); 
			String email1 = jsonPath.get("email");
			System.out.println(email1);	
			
			String footer1 = jsonPath.get("invoice_settings.footer");
			System.out.println(footer1);
			
			//testLevelLog.get().info(res.asString());
			
			//res.prettyPrint();
			// Stripe API sending 200 while creating new customer instead of standard 201.
			//Assert.assertEquals(200,res.getStatusCode());
		}
		
	     //  Parsing JSON Response and Using excel (xls) file and Generating Extent Report 
		// Warning : Method name and XLS sheet name should be same
		// https://stripe.com/docs/api/customers/list     
		@Test(dataProviderClass=DataProviderClass.class,dataProvider="dp",enabled=false)
		public void fetchCustomers(Hashtable<String, String> data)
		{
			/*
			 * URI - https://api.stripe.com/v1/customers
			 * MethodType-POST
			 * argument- email, description, balance
			 * auth- Basic auth- configProperty.getSecretKey()
			 */
			
			// Warning : Method name and XLS sheet name should be same
			
			testLevelLog.get().assignAuthor("Rajender");
			testLevelLog.get().assignCategory("Regression");
					
			RequestSpecification spec =  getRequestSpecification().log().all();	
			
	        System.out.println("=============Call to API 2 ================================");
			
	        // We are not passing "limit" so it will fetch 10 records
	        //https://stripe.com/docs/api/customers/list
	        Response response = spec.request(data.get("methodType"),data.get("endPoint"));
	        
	        // We get "data" as one of the key in response and it contains 10 records.That means its a JSON array element.
	        // To get only id's from each of the element in Array.
	        ArrayList<String> arrayList = response.path("data.id");
	        System.out.println("Id's in ArrayList=== " + arrayList);
	        
	        // To find size of each element (Basically number of attributes in each element) of array.
	        // Size (number of attributes) of first element in Array
            int numOfAttr = response.path("data[0].size()");
            System.out.println("numOfAttr=== " + numOfAttr);
            
            ArrayList<String> defaultSourceList = response.path("data.default_source");
            
            System.out.println("defaultSourceList=== " + defaultSourceList);
     
            for(int i=0; i<defaultSourceList.size();i++) {
            	
            	String cardNumber = response.path("data["+i+"].default_source"); 
            	
            	if(null != cardNumber && cardNumber.equals("card_1Hw2rzFa0vovYb1r8jfA3hxt")) {
            		
            		String customerID = response.path("data["+i+"].id"); 
            		System.out.println("customerID ::: " + customerID);
            		break;
            	}       	
            }	        
	        response.prettyPrint();				
		}
}
