package com.w2a.API_Batch3.TestUtils;

import java.lang.reflect.Method;
import java.util.Hashtable;

import org.testng.annotations.DataProvider;

import com.w2a.API_Batch3.setUp.APISetUp;

public class DataProviderClass2 extends APISetUp{
	
	@DataProvider(name="dp2")
	public  Object[][] getData(Method m) {

		System.out.println(configProperty);
		System.out.println("SheetName-->"+configProperty.getTestDataSheetName());
		String sheetName=configProperty.getTestDataSheetName();
		int rows = excel2.getRowCount(sheetName);//100
		String testName = m.getName();
		int testCaseRowNum = 1;

		for (testCaseRowNum = 1; testCaseRowNum <= rows; testCaseRowNum++) {

			String testCaseName = excel2.getCellData(sheetName, 0, testCaseRowNum);
			//System.out.println("TestCase name in excel2-->"+testCaseName);
			if (testCaseName.equalsIgnoreCase(testName))
				break;

		}// Checking total rows in test case
		System.out.println("TestCase starts from:- "+testCaseRowNum);

		int dataStartRowNum = testCaseRowNum + 2;//dataStartRowNum=3

		int testRows = 0;
		while (!excel2.getCellData(sheetName, 0, dataStartRowNum + testRows).equals("endOfTestData")) {

			testRows++;//1
		}
		// Checking total cols in test case

		//System.out.println("Total no of rows:"+testRows);
		int colStartColNum = testCaseRowNum + 1;//2
		int testCols = 0;

		while (!excel2.getCellData(sheetName, testCols, colStartColNum).equals("")) {

			testCols++;

		}
		//[2][1]
		
		Object[][] data = new Object[testRows][1];
		//object[][] data= new Object[2][1];
		//data[0][0]=
		//data[1][0]=

		int i = 0;
		for (int rNum = dataStartRowNum; rNum < (dataStartRowNum + testRows); rNum++) {

			Hashtable<String, String> table = new Hashtable<String, String>();

		
			for (int cNum = 0; cNum < testCols; cNum++) {

				String colName = excel2.getCellData(sheetName, cNum, colStartColNum);
				String testData = excel2.getCellData(sheetName, cNum, rNum);
				

				table.put(colName, testData);

			}

			data[i][0] = table;
			i++;

		}

		return data;
	}
}
