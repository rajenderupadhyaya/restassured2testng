package com.w2a.API_Batch3.pojo;

public class PersonDetailAllBooking {
	String firstname;
	String lastname;
	String checkin;
	String checkout;
	
	public PersonDetailAllBooking(String firstname, String lastname, String checkin, String checkout) {
		this.firstname=firstname;
		this.lastname=lastname; 
		this.checkin=checkin;
		this.checkout=checkout;			
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getCheckin() {
		return checkin;
	}

	public String getCheckout() {
		return checkout;
	}	
	
}
