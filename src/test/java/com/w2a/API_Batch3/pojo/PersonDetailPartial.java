package com.w2a.API_Batch3.pojo;

public class PersonDetailPartial {
	String firstname;
	String lastname;
	
	public PersonDetailPartial(String firstname, String lastname) {
		this.firstname=firstname;
		this.lastname=lastname;
			
	}
	
	public String getFirstname() {
		return firstname;
	}
	
	public String getLastname() {
		return lastname;
	}	
}
