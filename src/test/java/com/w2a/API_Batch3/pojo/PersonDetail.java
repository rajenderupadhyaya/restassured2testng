package com.w2a.API_Batch3.pojo;

// Sychronization Test
public class PersonDetail {
	String firstname;
	String lastname;
	int totalprice;
	boolean depositpaid;
	BookingDates bookingdates;
	
	String additionalneeds;
	
	public PersonDetail(String firstname, String lastname, int totalprice, boolean depositpaid, String additionalneeds) {
		this.firstname=firstname;
		this.lastname=lastname;
		this.totalprice=totalprice;
		this.depositpaid=depositpaid;
		this.additionalneeds=additionalneeds;		
	}
	
	public String getFirstname() {
		return firstname;
	}
	
	public String getLastname() {
		return lastname;
	}
	
	public int getTotalprice() {
		return totalprice;
	}
	
	public boolean isDepositpaid() {
		return depositpaid;
	}
	
	public BookingDates getBookingdates() {
		return bookingdates;
	}
	
	public void setBookingdates(BookingDates bookingdates) {
		this.bookingdates = bookingdates;
	}
	
	public String getAdditionalneeds() {
		return additionalneeds;
	}
}
